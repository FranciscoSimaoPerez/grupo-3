
//**************** Script to hide the error message ***********************
$('.close').on('click', function(event){
	$(".msg-error-js").hide();
});
//**************** Script to hide the error message ***********************



//***************** Script to insert disponibilities and update the dropdown **************
$(".msg-error-js").hide();
$('.bt-submit-js').on('click', function(event){
  event.preventDefault();
    var base_url = $('#base_url').val();
    $.post(base_url + '/index.php/adicionar_c/disponibilidade', $('#form-create-usuario').serialize(), function(response){
      console.log(response);
      if(response.success) {
        alert('Datos inseridos');
        $('#opt-disp').append(new Option($('#disponibilidade').val(), response.insert_id));
        $('#disponibilidade').val("");
        $(".msg-error-js").hide();
      }else if(response.failed){
        alert('Campo vacio');
        $(".list-errors").html(response.failed);
        $(".msg-error-js").show(response);
      }
    }, 'json');  
  });
//***************** Script to insert disponibilities and update the dropdown **************


//***************** Script to insert locations and update the dropdown **************
$(".msg-error-js").hide();
$('#loc').on('click', function(event){
  event.preventDefault();
    var base_url = $('#base_url').val();
    $.post(base_url + '/index.php/adicionar_c/localization', $('#form-create-localization').serialize(), function(response){
      console.log(response);
      if(response.success) {
        alert('Datos inseridos');
        $('#opt-loc').append(new Option($('#input-loc').val(), response.insert_id));
        $('#input-loc').val("");
        $(".msg-error-js").hide();
      }else if(response.failed){
        alert('Campo vacio');
        $(".list-errors").html(response.failed);
        $(".msg-error-js").show(response);
      }
    }, 'json');  
  });
//***************** Script to insert locations and update the dropdown **************


//***************** Script to insert categories(types) and update the dropdown **************
 $(".msg-error-js").hide();
        $('#reg-type').on('click', function(event){
          event.preventDefault();
            var base_url = $('#base_url').val();
            $.post(base_url + '/index.php/adicionar_c/type', $('#form-create-types').serialize(), function(response){
              console.log(response);
              if(response.success) {
                alert('Datos inseridos');
                $('#opt-types').append(new Option($('#type').val(), response.insert_id));
                $('#type').val("");
                $(".msg-error-js").hide();
              }else if(response.failed){
                alert('Campo vacio');
                $(".list-errors").html(response.failed);
                $(".msg-error-js").show(response);
              }
            }, 'json');  
          });
//***************** Script to insert categories(types) and update the dropdown **************




//***************** Script to insert brands and update the dropdown **************
$(".msg-error-js").hide();
        $('#btn-brand').on('click', function(event){
          event.preventDefault();
            var base_url = $('#base_url').val();
            $.post(base_url + '/index.php/adicionar_c/brand', $('#form-create-brand').serialize(), function(response){
              console.log(response);
              if(response.success) {
                alert('Datos inseridos');
                $('#opt-brand').append(new Option($('#input-brand').val(), response.insert_id));
                $('#input-brand').val("");
                $(".msg-error-js").hide();
              }else if(response.failed){
                alert('Campo vacio');
                $(".list-errors").html(response.failed);
                $(".msg-error-js").show(response);
              }
            }, 'json');  
          });
//***************** Script to insert brands and update the dropdown **************



 //************************** Script to insert models **************************

$('#create-model').on('click', function(event){
      event.preventDefault();
        var base_url = $('#base_url').val();
        $.post(base_url + '/index.php/adicionar_c/create_model', $('#form-create-model').serialize(), function(response){
          console.log(response);
          if(response.success) {
            alert('Datos inseridos');
            $('#modelo').val("");
            $('#opt-types').val("");
            $('#opt-brand').val("");
            $(".msg-error-js").hide();
          }else if(response.failed){
            alert('Campo vacio');
            $(".list-errors").html(response.failed);
            $(".msg-error-js").show(response);
          }
        }, 'json');  
      });

 //************************** Script to insert models **************************


//************************** Script to insert colors **************************

$('#btn-color').on('click', function(event){
  event.preventDefault();
    var base_url = $('#base_url').val();
    $.post(base_url + '/index.php/adicionar_c/colors', $('#form-create-colors').serialize(), function(response){
      console.log(response);
      if(response.success) {
        alert('Datos inseridos');
        $('#opt_colors_1').append(new Option($('#input_color').val(), response.insert_id));
        $('#input-color').val("");
        $(".msg-error-js").hide();
      }else if(response.failed){
        alert('Campo vacio');
        $(".list-errors").html(response.failed);
        $(".msg-error-js").show(response);
      }
    }, 'json');  
  });

 //************************** Script to insert models **************************



 //************************** Script to insert equipments **************************

$('#create_equipment').on('click', function(event){
      event.preventDefault();
        var base_url = $('#base_url').val();
        $.post(base_url + '/index.php/adicionar_c/create_equipment', $('#form-create-equipment').serialize(), function(response){
          console.log(response);
          if(response.success) {
            alert('Datos inseridos');
            $('#serial').val("");
            $('#opt-model').val("");
            $('#opt-loc').val("");
            $('#opt-disp').val("");
            $('#opt_colors_1').val("");
            $(".msg-error-js").hide();
          }else if(response.failed){
            alert('Os seus dados não foram guardados');
            $('#serial').val("");
            $(".list-errors").html(response.failed);
            $(".msg-error-js").show(response);
          }
        }, 'json');  
      });

 //************************** Script to insert models **************************


//****************Script for datatables *************
$(document).ready(function() {
    $('#book-table').DataTable({
      "ajax": {
        url : "requisition_page",
        type : 'GET'
      },
    });
});
//****************Script for datatables *************

//****************Script for datatables Reservation By equipment*************
$(document).ready(function() {
    $('#equipaments').DataTable({
      "ajax": {
        url : "req_equipament_page",
        type : 'GET'
      },
    });
});

//****************Script for datatables Location *************
$(document).ready(function() {
    $('#location').DataTable({
      "ajax": {
        url : "req_location_page",
        type : 'GET'
      },
    });
});
