<?php
declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common class for developing web apps for acin hub. This main
 * library contains all commun functions required for developing
 * acin hub web apps.
 * 
 */
class Acin_hub{

	/**
	 * Contains the Code Igniter unique instance
	 */
	private $CI;

	public function __construct(){
		$this->CI =& get_instance();
	

		//Load acin_hub helper
		$this->CI->load->helper('acin_hub');
		$this->CI->load->helper('cookie');
		$this->CI->load->library('session');


		//Set up configuration file
		if($this->CI->config->load('acin_hub')){
			if(	
				is_null($this->CI->config->item('acin_hub_home'))  ||
				is_null($this->CI->config->item('acin_hub_logout'))||
				is_null($this->CI->config->item('acin_hub_login')) ||
				is_null($this->CI->config->item('acin_hub_web_app_id'))   ){
				throw new Exception("Error Processing Request: acin_hub.php file is missing some parameters.", 1);
		}
	}else{
		throw new Exception("Error Processing Request: acin_hub.php config file missing.", 1);
	}
	$this->CI->load->database();
}


	/**
	 * Verify credentials from the user to log in acin hub
	 *
	 * @param      string   $username      The username
	 * @param      string   $password      The password
	 * @param      string   $redirect_ok   URL that will redirect the user if the login is successfull
	 * @param      string   $redirect_nok  URL that will redirect the user if the login fails
	 *
	 * @return     boolean  if no redirect is provided, will return true if successfull false otherwise
	 */
	public function acin_hub_login(string $username, string $password, bool $remember_me = false, string $redirect_ok=""): bool{

		//if no username or password, always return false
		if(empty($username) || empty($password)){
			return false;
		}

		//Get stored hash from username
		$stored_hash_password = $this->get_password_hash_from_username($username);

		if($stored_hash_password){
			if($stored_hash_password && password_verify($password, $stored_hash_password)){
				//check for the remember me option
				$this->configure_remember_cookie($remember_me);

				//Build session temporary cache with user data
				$session_data = (array) $this->get_user($username);
				$session_data['is_logged_in'] = true;
				$this->CI->session->set_userdata($session_data);

				//Check for link to redirect to if successfull login
				if(!empty($redirect_ok)){
					redirect($redirect_ok,'location',302);
				}else{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Configures session cookie on the client side according to if the user
	 * has set the remind me option.
	 * 
	 * By default cookie will expire upon 7200 seconds, if remeber me is set, it will
	 * expire after one month
	 * 
	 * Session id will still be renewed according to the internal settings of CI
	 *
	 * @param      bool  $remember_me  If remember_me is true an long session will be built
	 */
	private function configure_remember_cookie($remember_me){
		if ($remember_me) {
                $this->CI->session->sess_expiration = $this->config->item('acin_hub_remember_me_cookie_expirancy'); 
                $this->CI->session->sess_expire_on_close = FALSE;
            }
        }

	/**
	 * Gets the password hash from username.
	 *
	 * @param      <type>  $username  The username
	 *
	 * @return     <type>  The password hash from username.
	 */
	private function get_password_hash_from_username($username){
		$row = $this->CI->db->select('password')
		->from("ah_utilizadores_login")
		->where("username",$username)
		->where("b_removido",0)
		->get()->row();
		return $row->password ?? null;
	}

	/**
	 * Returns logged in user profile in a stdClass object. This object will includ all 
	 *  info about personal and public profile along the user itself.
	 * 
	 */
	public function get_user_profile(): stdClass{
		if($this->is_user_logged_in()){
			$id_user = $this->CI->session->userdata("id");
			$user = $this->CI->db->select('guu.id as id_utilizador, ahup.id as id_utilizador_perfil, guu.nome as nome, guu.email_profissional, guu.ext_telf, guu.genero, GROUP_CONCAT(DISTINCT(gud.nome) )as departamentos,, guf.nome as filial, ahup.email_pessoal, ahup.telefone_pessoal, ahup.sobre_mim, ahup.data_nascimento,ahup.b_bolo_aniversario, CONCAT_WS(\'\',aha.url,aha.nome) AS url_foto, ahe.nome as estado_civil, GROUP_CONCAT(DISTINCT(ahtag.nome)) as tags,GROUP_CONCAT(DISTINCT(lingua.nome)) as linguas')
			->from('gu_utilizadores guu')
			->join('ah_utilizadores_login ahul','ahul.id_utilizadores = guu.id','left')
			->join('gu_filiais guf','guf.id = guu.id_filial','left')
			->join('gu_utilizador_departamento guud','guud.id_utilizador = guu.id','left')
			->join('gu_departamentos gud','gud.id = guud.id_departamento','left')
			->join('ah_utilizadores_perfis ahup','ahup.id_utilizadores = guu.id','left')
			->join('ah_anexos aha','aha.id = ahup.id_foto_perfil','left')
			->join('ah_estados_civis ahe','ahe.id = ahup.id_estado_civil','left')
			->join('ah_perfis_has_tags ahpt','ahpt.id_utilizadores_perfis = ahup.id','left')
			->join('ah_tags ahtag','ahtag.id = ahpt.id_tag','left')
			->join('ah_perfis_has_lingua phl','phl.id_utilizadores_perfis = ahup.id','left')
			->join('ah_lingua lingua','lingua.id = phl.id_lingua','left')
			->group_by('ahul.id')
			->where('guu.id',$id_user)->get()->row();
			return $user;
		}
		return null;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return     boolean  The user.
	 */
	private function get_user(string $username=""):stdClass{
		return $this->CI->db->select("guu.id,nome,email_profissional,username")
		->from("ah_utilizadores_login ahul")
		->join("gu_utilizadores guu","guu.id=ahul.id_utilizadores")
		->where("ahul.username",$username)->get()->row();
	}

	/**
	 * Gets details about the current app.
	 *
	 * @return    array returns the current app profile details
	 */
	public function get_app_profile(){
		$app_id = $this->CI->config->item('acin_hub_web_app_id');
		$details = $this->CI->db->from("ah_web_app")->where("b_removida","0")
		->where("id",$app_id)->get()->row();
		return $details;
	}
	
	/**
	 * Determines if user logged in by evaluating the user toker cookie on the browser
	 * and if the token is valid on the database.
	 * 
	 */
	public function is_user_logged_in(): bool{
		return $this->CI->session->userdata("is_logged_in")??false;
	}
	
	/**
	 * Gets the user access level for the current web app. Users can have 4 different levels
	 * 3 - Administrator
	 * 2 - Content Manager
	 * 1 - User
	 * 0 - not assigned to the app
	 * 
	 * Web app id must be set on config/acin_hub.php
	 *
	 * @param      <type>  $app_id  The application identifier
	 */
	public function get_user_access_level():int{
		if($this->is_user_logged_in()){
			$user_webapp = $this->CI->db->select('gun.nivel_acesso as nivel_acesso')
			->from('gu_utilizador_webapp guw')
			->join('gu_niveis_acesso gun','gun.id = guw.id_niveis_acesso','left')
			->where('guw.id_utilizador',$this->CI->session->userdata('id'))
			->where('guw.id_web_app',$this->CI->config->item('acin_hub_web_app_id'))
			->where('guw.b_removido',"0")
			->get()->row();
			if($user_webapp){
				return intval($user_webapp->nivel_acesso);
			}
		}
		return 0;
	}



	/**
	 * Gets the user access level name.
	 */
	public function get_user_access_level_name():string{
		if($this->is_user_logged_in()){
			$user_webapp = $this->CI->db->select('gun.nome as nome')
			->from('gu_utilizador_webapp guw')
			->join('gu_niveis_acesso gun','gun.id = guw.id_niveis_acesso','left')
			->where('guw.id_utilizador',$this->CI->session->userdata('id'))
			->where('guw.id_web_app',$this->CI->config->item('acin_hub_web_app_id'))
			->where('guw.b_removido',"0")
			->get()->row();
			if($user_webapp){
				return $user_webapp->nome;
			}
		}
		return "";
	}

	/**
	 * Eliminates current user session
	 */
	public function logout(){
		$this->CI->session->sess_destroy();
		redirect($this->CI->config->item('acin_hub_home'));
	}

	/**
	 * Redirects user to home on acin hub
	 */
	public function home(){
		redirect($this->CI->config->item('acin_hub_home'));
	}
}