<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Defines the numeric value for user access level admin in a specified app**/
const USER_ADMIN           = 3;

/** Defines the numeric value for user access level content manager in a specified app**/
const USER_CONTENT_MANAGER = 2;

/** Defines the numeric value for user access level basic in a specified app**/
const USER_BASIC           = 1;

/** Defines the numeric value for user access level not allowed in a specified app**/
const USER_INVALID         = 0;

/** Contains the name for the cookie to be created after successfull login**/
const LOGIN_COOKIE_NAME    = "acin_hub_token";

const ACIN_HUB_SERVER_URL  = "http://192.168.1.100/hub/";