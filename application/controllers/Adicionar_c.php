
<?php
require_once APPPATH. 'controllers/Login_c.php';
	class Adicionar_c extends login_c {


    function __construct()
    {
        parent::__construct();
        $this->load->model(array('equipments_m', 'reserve_m'));
        $this->load->helper('date');

        $this->data['access_level'] = $this->access_level;
        $this->data['user'] = $this->user->nome;
        $this->data['user_id'] = $this->user->id_utilizador;

        if($this->access_level!=3){
            redirect(base_url('../'));
        }   
    }



    public function reserva() {
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/reserva');
        $this->load->view('Templates/footer');
    }

    public function requisicoes() {
		
        $data['list_brands'] = $this->
        $this->template_m->template('pages/requisicoes', $data);

    	}

    public function req_equipament() {
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/req_equipament');
        $this->load->view('Templates/footer');
    }

    public function loc_equipament() {
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/location');
        $this->load->view('Templates/footer');
    }

    

//********************************************************************************
    //Function to register the receive of the equipments
    public function receive() {
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/receive');
        $this->load->view('Templates/footer');
    }


    //Function to show the equipments
    public function add() 
    {
        $data = array(
            'availability' => $this->equipments_m->show_availabilities(),
            'location'     => $this->equipments_m->show_locations(), 
            'model'        => $this->equipments_m->show_models(),
            'colors'       => $this->equipments_m->show_colors()
        );



		$this->load->view('Templates/header', $this->data);
        $this->load->view('pages/add', $data);
        $this->load->view('Templates/footer');
    }

    //Function to save the diferents disponibilities
    public function disponibilidade()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('disponibilidade', 'Disponibilidade', 'required');
        //Set message
        $this->form_validation->set_message('Disponibilidade', 'O campo %s é necessário');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "availability" => $post['disponibilidade']
                ];
                //Insert the disponibility
                $insert_id = $this->equipments_m->insert_disponibility($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to save the diferents localizations
    public function localization()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('local', 'Localização', 'required');
        //Set message
        $this->form_validation->set_message('Localização', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "location" => $post['local']
                ];
                //Insert the disponibility
                $insert_id = $this->equipments_m->insert_localization($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to create a the models
    public function show_model_properties()
    {
        $data = array(
            'type'  => $this->equipments_m->show_types(), 
            'brand' => $this->equipments_m->show_brands()
        );

        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/create_models', $data);
        $this->load->view('Templates/footer');
    }

    //Function to save the diferent types
    public function type()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('type', 'Categoria', 'required');
        //Set message
        $this->form_validation->set_message('Categoria', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "type" => $post['type']
                ];
                //Insert the type
                $insert_id = $this->equipments_m->insert_type($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to save the diferent brands
    public function brand()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('brand', 'Marcas', 'required');
        //Set message
        $this->form_validation->set_message('Marcas', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "brand" => $post['brand']
                ];
                //Insert the type
                $insert_id = $this->equipments_m->insert_brand($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to save the diferent colors
    public function colors()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('input_color', 'Color', 'required');
        //Set message
        $this->form_validation->set_message('Color', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "color" => $post['input_color']
                ];
                //Insert the type
                $insert_id = $this->equipments_m->insert_colors($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to save the diferent model
    public function create_model()
    {
        //Receive the parameters
        $post = $this->input->post();
        //Form validation
        $this->form_validation->set_rules('modelo', 'Modelo', 'required');
        $this->form_validation->set_rules('opt-brand', 'Marcas', 'required');
        $this->form_validation->set_rules('opt-types', 'Types', 'required');
        //Set message
        $this->form_validation->set_message('Marcas', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data = [
                    "model"    => $post['modelo'],
                    "type_id"  => $post['opt-types'],
                    "brand_id" => $post['opt-brand']
                ];
                //Insert the type
                $insert_id = $this->equipments_m->insert_models($data);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Field empty', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

    //Function to save the equipments
    public function create_equipment()
    {
        //Receive the parameters
        $post = $this->input->post();



        
        

        //Form validation
        $this->form_validation->set_rules('opt-model', 'Modelo', 'required');
        $this->form_validation->set_rules('opt-loc', 'Localization', 'required');
        $this->form_validation->set_rules('opt-disp', 'Availability', 'required');
        $this->form_validation->set_rules('opt_colors_1', 'Colors', 'required');
        $this->form_validation->set_rules('serial', 'Serial number', 'required|is_unique[er_equipments.serial_n]');
        //Set message
        $this->form_validation->set_message('Modelo', 'El campo %s es necesario');
            //Validation
            if ($this->form_validation->run()) {
                $data_equip = [
                    "availability_id"    => $post['opt-disp'],
                    "location_id"  => $post['opt-loc'],
                    "model_id" => $post['opt-model'],
                    "serial_n" => $post['serial']

                ];
                $data_color = [
                    'model_id' => $post['opt-model'],
                    'color_id' => $post['opt_colors_1']
                ];
                //Insert the equipment
                $insert_id = $this->equipments_m->insert_equipment($data_equip);
                $this->equipments_m->insert_model_color($data_color);
                //If the insertion it's ok
                if ($insert_id) {
                    echo json_encode(array('success' => true, 'messages' => array(), 'insert_id' => $insert_id));
                }
                else{
                   echo json_encode(array('success' => true, 'messages' => array()));
                }
            }
            else
            {
                echo json_encode(array('failed' => 'Os seus dados não foram guardados', 'messages' => array(validation_errors("<li>","</li>"))));
            }
    }

 //Function to show all the locations of the equipments
    public function req_location_page()
    {
        //datatables variables
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $locations = $this->reserve_m->location_equipament();
        
        $data = array();

        foreach ($locations->result() as $d) {
            $data[] = array(
                $d->type,
                $d->brand,
                $d->model,
                $d->nome_filial
            );
            
        }
        $output = array(
                "draw" => $draw,
                "recordsTotal" => $locations->num_rows(),
                "recordsFiltered" => $locations->num_rows(),
                "data" => $data
            );
          echo json_encode($output);
          exit();
    }

    //Function to show all the requisitions of the user by equipaments
    public function req_equipament_page()
    {
        //datatables variables
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $equipament = $this->reserve_m->requisitions_equipament();
        
        $data = array();

        foreach ($equipament->result() as $e) {
            $data[] = array(
                $e->type,
                $e->model,
                $e->serial_n,
                date('d-m-y', strtotime($e->date_ini)),
                date('d-m-y', strtotime($e->date_end)),
                $e->motivo,
                $e->date_delivery,
                //date('d-m-y', strtotime($e->date_delivery)),
                $e->username
            );
            
        }
        $output = array(
                "draw" => $draw,
                "recordsTotal" => $equipament->num_rows(),
                "recordsFiltered" => $equipament->num_rows(),
                "data" => $data
            );
          echo json_encode($output);
          exit();
    }

    private function informacao_utilizador(){
        $user = new stdClass();
        $data = $this->acin_hub->get_user_profile();
        $user->id_utilizador_perfil=$data->id_utilizador_perfil;

        // emoçoes
        $user->ultima_emocao    = $this->user_emotions->ultima_emocao($data->id_utilizador_perfil);
        $user->emocoes          = $this->user_emotions->todas_emocoes();

        // perfil->nome; perfil->foto
        $user->nome             = $this->acin_hub->get_user_profile()->nome;
        $user->url_foto         = $this->acin_hub->get_user_profile()->url_foto;

        // nivel de acesso
        $user->nivel_acesso     = $this->acin_hub->get_user_access_level_name();

        // as minhas web apps
        $user->minhas_webapps   = $this->webapps_model->minhaWebapp($data->id_utilizador);
        return $user;
    }

    public function receive_list()
  {
      $list = $this->reserve_m->get_data_receive();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $equip) {
          $no++;
          $row = array();
          $row[] = $equip->brand;
          $row[] = $equip->model;
          $row[] = $equip->color;
          $row[] = $equip->type;
          $row[] = $equip->location;
          $row[] = $equip->availability;
          //add html for action
          $row[] = '<a class="btn btn-sm btn-primary" href="receive_equipment/'.$equip->id.'" title="Recebe" ><i class="glyphicon glyphicon-pencil"></i>Receive</a>';
          
          $data[] = $row;
      }
      $output = array(
                      "draw" => $_POST['draw'],
                      "recordsTotal" => $this->reserve_m->count_all(),
                      "recordsFiltered" => $this->reserve_m->count_filtered(),
                      "data" => $data,
              );
      //output to json format
      echo json_encode($output);
  }

  public function receive_equipment() {

    $id   = $this->uri->segment(3);
    $date = date('Y-m-d H:i:s');

    $this->equipments_m->reg_equipment($id);
    $this->equipments_m->reg_reservation($id,$date);



  }
    
}

