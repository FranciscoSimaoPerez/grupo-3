<?php 	
require_once APPPATH. 'controllers/Login_c.php';
	class Reservar_c extends Login_c {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('reserve_m', 'equipments_m') );
        
        $this->data['access_level'] = $this->access_level;
        $this->data['user'] = $this->user->nome;
        $this->data['user_id'] = $this->user->id_utilizador;


        
    }
    //Public variable
    public $variable = "Equipamentos";

    
    //Function for show to the users all the categories
    public function index() 
    {
        
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/home');
        $this->load->view('Templates/footer');
      
  
          
    }

    //Function for show the equipments to reserve
    public function reservar() 
    {
        $id = $this->uri->segment(3);
        
        $data = array(
            'equipments' => $this->reserve_m->show_to_reserve($id)
        );

        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/reserva',$data);
        $this->load->view('Templates/footer');
    }

    //Function to create the reserve of the elements
    public function create_reserve()
    {   
        $id = $this->uri->segment(3);
        $data = array(
           'equip' => $this->reserve_m->show_to_reserve($id)
        );
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/insert_reserve', $data);
        $this->load->view('Templates/footer');

        
   }

   //Function to insert the reservation
   public function insert_reservation()
   {
        $this->form_validation->set_rules('date_in', 'Initial date', 'required');
        $this->form_validation->set_rules('date_out', 'End date', 'required');
        $this->form_validation->set_rules('motivo', 'Motivo', 'required');

        $this->form_validation->set_message('motivo', 'Error Message');
        
        if ($this->form_validation->run()) 
        {
            $data = array(
            'date_ini'  => date("Y-m-d", strtotime($this->input->post('date_in'))),
            'date_end'  => date("Y-m-d" , strtotime($this->input->post('date_out'))),
            'motivo'    => $this->input->post('motivo'),
            'equipment_id' => $this->uri->segment(3),
            'user_id' => $this->user->id_utilizador

            );
            
            $this->equipments_m->update_equip_reserve($data);
            if ($this->reserve_m->insert_reservation($data)) 
            {
                $this->session->set_flashdata('reservation_success', ' <i class="fas fa-check-circle"></i> Equipamento Reservado Com Sucesso!');
                redirect('reservar_c');
            } 
            else 
            {
                $this->session->set_flashdata('reservation_error', ' <i class="fas fa-check-circle"></i> Erro ao Reservar!');
                redirect('reservar_c');
            }
        }
        else 
        {
            $this->session->set_flashdata('reservation_error', ' <i class="fas fa-check-circle"></i> Erro ao Reservar!');
            redirect('reservar_c');
        }
    }
    
    //Function to show all the requisitions of the users
    public function requisitions() 
    {
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/requisitions');
        $this->load->view('Templates/footer');
    }

    public function requisition_page()
    {
        //datatables variables
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $requisitions = $this->reserve_m->requisitions();
        
        $data = array();

        foreach ($requisitions->result() as $r) {
            $data[] = array(
                $r->brand,
                $r->model,
                date('d-m-y', strtotime($r->date_ini)),
                date('d-m-y', strtotime($r->date_end)),
                $r->motivo
            );
            
        }
        $output = array(
               "draw" => $draw,
                "recordsTotal" => $requisitions->num_rows(),
                 "recordsFiltered" => $requisitions->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }

//**************************************************************
   public function ajax_list()
  {
      $list = $this->reserve_m->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $equip) {
          $no++;
          $row = array();
          $row[] = $equip->brand;
          $row[] = $equip->model;
          $row[] = $equip->color;
          $row[] = $equip->type;
          $row[] = $equip->location;
          $row[] = $equip->availability;
          //add html for action
          if ($equip->availability_id == 1) {
            $row[] = '<a class="btn btn-sm btn-primary" href="reservar_c/create_reserve/'.$equip->id.'" title="Reservar" ><i class="glyphicon glyphicon-pencil"></i>Reservar</a>';
          }
          
          $data[] = $row;
      }
      $output = array(
                      "draw" => $_POST['draw'],
                      "recordsTotal" => $this->reserve_m->count_all(),
                      "recordsFiltered" => $this->reserve_m->count_filtered(),
                      "data" => $data,
              );
      //output to json format
      echo json_encode($output);
  }


//**************************************************************

}

