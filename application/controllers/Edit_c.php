<?php
require_once APPPATH. 'controllers/Login_c.php';
  
  class Edit_c extends login_c { 

	public function __construct()
	{
        parent::__construct();
        $this->load->model('view_m','model_view');
		    $this->load->model(array('equipments_m', 'reserve_m'));

        $this->data['access_level'] = $this->access_level;
        $this->data['user'] = $this->user->nome;
        $this->data['user_id'] = $this->user->id_utilizador;

        if($this->access_level!=3){
            redirect(base_url('../'));
        }  
    }

    public function index(){
        $this->load->library('table');
        
        
        $this->model_view->view_admin('pages/edit', $this->data);
    }

    public function save_equipment_changes($idCrip){
		$this->form_validation->set_rules('select-availability', 'Disponibilidade', 'required');
		$this->form_validation->set_rules('select-location', 'Localização', 'required');
		$this->form_validation->set_rules('textarea-observation', 'Observações', 'required');

		$availability_id = $this->input->post('select-availability');
		$location_id = $this->input->post('select-location');
		$observation = $this->input->post('textarea-observation');

		if($this->form_validation->run() == FALSE){
			$this->edit($idCrip);
		} else {
			$id = $this->input->post('txt-id');
			if($this->model_equipments->save_equipment_changes($id, $availability_id, $location_id, $observation)){
				redirect(base_url('admin/equipments'));
			} else {
				echo "Houve um erro no sistema!";
			}
		}
		
    }

    public function edit_equip($id){
        $data['locations'] = $this->equipments_m->show_acin_locations();
        $data['availabilities'] = $this->equipments_m->show_availabilities();
        $data['equipment'] = $this->reserve_m->show_equipment($id);
        $this->load->view('Templates/header', $this->data);
        $this->load->view('pages/edit-equip', $data);
        $this->load->view('Templates/footer');
    }

     public function delete_equip($id){
    $this->load->library('table');
    $data['equipment']=$this->reserve_m->show_equipment($id);
    $this->load->view('Templates/header', $this->data);
    $this->load->view('pages/delete-equip', $data);
    $this->load->view('Templates/footer');
  }

  public function confirm_delete_equip($id)
  {
    if($this->equipments_m->delete($id)){
      redirect(base_url('edit'));
    } else {
      echo "Houve um erro no sistema!";
    }
  }

    
    //**************************************************************
   public function ajax_list()
   {

       $list = $this->reserve_m->get_datatables();
       $data = array();
       $no = $_POST['start'];
       foreach ($list as $equip) {
           $no++;
           $row = array();
           $row[] = $equip->brand;
           $row[] = $equip->model;
           $row[] = $equip->color;
           $row[] = $equip->type;
           $row[] = $equip->location;
           $row[] = $equip->availability;
           $row[] = $equip->id;
           //add html for action
           if ($equip->availability_id == 1) {
             $row[] = '<a class="btn btn-sm btn-warning" href="edit_c/edit_equip/'.$equip->id.'">Editar</a>
                       <a class="btn btn-sm btn-danger" href="edit_c/delete_equip/'.$equip->id.'">Eliminar</a> ';
           }
           
           $data[] = $row;
       }
       $output = array(
                       "draw" => $_POST['draw'],
                       "recordsTotal" => $this->reserve_m->count_all(),
                       "recordsFiltered" => $this->reserve_m->count_filtered(),
                       "data" => $data,
               );
       //output to json format
       echo json_encode($output);
   }
 
    
}