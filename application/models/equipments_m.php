<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipments_m extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
		//$this->load->database();
	}

	//Function to show all the locations of acin in the world
	public function show_locations() {
		
		$this->db->select('*');
		$this->db->from('fc_filial');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show all the locations of acin in the world
	public function show_acin_locations() {
		return $this->db->select()->get('er_acin_locations')->result();
	}


	//Function to show the actions in the database
	public function show_actions()
	{
		$this->db->select('*');
		$this->db->from('er_actions');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show all the availabilities in the database
	public function show_availabilities()
	{
		$this->db->select('*');
		$this->db->from('er_availabilities');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show all the brands in the database
	public function show_brands()
	{
		$this->db->select('*');
		$this->db->from('er_brands');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show colors
	public function show_colors()
	{
		$this->db->select('*');
		$this->db->from('er_colors');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show all the models of equipments
	public function show_models()
	{
		$this->db->select('*');
		$this->db->from('er_models');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to show all the types of equipments
	public function show_types()
	{
		
		$this->db->select('*');
		$this->db->from('er_types');
		$query = $this->db->get();
		return $query->result();
	}

	//Function to insert types of equipments
	public function insert_disponibility($info)
	{
		$this->db->insert('er_availabilities', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert localizations
	public function insert_localization($info)
	{
		$this->db->insert('fc_filial', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert localizations
	public function insert_type($info)
	{
		$this->db->insert('er_types', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}
	//Function to insert localizations
	public function insert_brand($info)
	{
		$this->db->insert('er_brands', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert modelos
	public function insert_models($info)
	{
		$this->db->insert('er_models', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert colors
	public function insert_colors($info)
	{
		$this->db->insert('er_colors', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert equipment
	public function insert_equipment($info)
	{
		$this->db->insert('er_equipments', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to insert models-colors
	public function insert_model_color($info)
	{
		$this->db->insert('er_models_has_colors', $info);
		if ($this->db->insert_id()) 
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
		
	}

	//Function to update the availability of the equip befores reserve
	public function	update_equip_reserve($data)
	{
		$this->db->set('availability_id', 2, FALSE);
		$this->db->where('id', $data['equipment_id']);
		$this->db->update('er_equipments');
	}

	//Function to delete(hide) equipment
	public function delete($id){
		$data['b_delete'] = 1;
        return $this->db->where('md5(id)',$id)
                        ->update('er_equipments', $data);
    }

    //Function to save changes of equipment made by ADMIN
    public function save_equipment_changes($id, $availability_id, $location_id, $observation){
        $data['availability_id'] = $availability_id;
        $data['location_id'] = $location_id;
        //$data['observation'] = $observation;
        return $this->db->where('md5(id)', $id)
                        ->update('er_equipments',$data);
    }

    public function reg_equipment($id) {
    	$this->db->where('id', $id)
    			 ->update('er_equipments', array('availability_id' => 1));
    }

    public function reg_reservation($id,$date){
    	$this->db->where('equipment_id', $id)
    		     ->update('er_reservations', array('date_delivery' => $date));	
    }

	

}
