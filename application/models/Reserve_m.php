<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserve_m extends CI_Model 
{
	//*********************************************
	var $table = 'er_equipments';
    var $column_order = array('availability','location','model', 'type', 'brand', 'color'); //set column field database for datatable orderable
    var $column_search = array('availability','location','model', 'type', 'brand', 'color'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order 


	//*********************************************

	function __construct()
	{
		parent::__construct();
		
	}

	public function show_equipment($id)
	{
		return $this->db->select('A.id as id_equip,
						   A.availability_id,
						   A.location_id,
						   A.model_id, 
						   A.b_delete, 
						   B.id, 
						   B.availability, 
						   C.location, 
						   D.model, 
						   E.id, 
						   E.type, 
						   F.brand, 
						   H.color')
						->from('er_equipments as A')
						->join('er_availabilities as B', 'A.availability_id=B.id', 'inner')
						->join('er_acin_locations as C', 'A.location_id=C.id', 'inner')
						->join('er_models as D', 'A.model_id=D.id', 'inner')
						->join('er_types as E', 'D.type_id=E.id', 'inner')
						->join('er_brands as F', 'D.brand_id=F.id', 'inner')
						->join('er_models_has_colors as G', 'D.id=G.model_id', 'inner')
						->join('er_colors as H', 'G.color_id=H.id', 'inner')
						->where('A.id', $id)
						->where('A.b_delete', 0)
						->group_by('A.id')
						->get()->row();
	}



	//Function to show the equipments to reserve
	public function show_to_reserve($id)
	{
		$this->db->select('A.id as id_equip, A.availability_id, A.location_id, A.model_id, A.b_delete, B.id, B.availability, C.location, D.model, E.id, E.type, F.brand, H.color');
		$this->db->from('er_equipments as A');
		$this->db->join('er_availabilities as B', 'A.availability_id=B.id', 'inner');
		$this->db->join('er_acin_locations as C', 'A.location_id=C.id', 'inner');
		$this->db->join('er_models as D', 'A.model_id=D.id', 'inner');
		$this->db->join('er_types as E', 'D.type_id=E.id', 'inner');
		$this->db->join('er_brands as F', 'D.brand_id=F.id', 'inner');
		$this->db->join('er_models_has_colors as G', 'D.id=G.model_id', 'inner');
		$this->db->join('er_colors as H', 'G.color_id=H.id', 'inner');
		$this->db->where('A.id', $id);
		$this->db->where('A.b_delete', 0);
		$this->db->group_by('A.id');
		$query = $this->db->get();
		return $query->result();
	}



	//Show all the requisitions of the users
	public function requisitions(){
			$this->db->select("er_r.id , er_r.equipment_id,
			er_r.date_ini, er_r.date_end, er_r.motivo, er_b.brand, er_m.model, GROUP_CONCAT(er_c.color SEPARATOR '||' ) colors");
			$this->db->from('er_reservations er_r');
			$this->db->join('er_equipments er_e', 'er_e.id = er_r.equipment_id');
			$this->db->join('er_models er_m','er_m.id = er_e.model_id');
			$this->db->join('er_brands er_b','er_b.id = er_m.brand_id');
			$this->db->join('er_models_has_colors er_m_c', 'er_m_c.model_id = er_m.id');
			$this->db->join('er_colors er_c', 'er_c.id = er_m_c.color_id');
			$this->db->group_by('er_r.id');
			return $this->db->get();
		    //return $query->result();
	}

	//Show all the requisitions of the users by equipament
	public function requisitions_equipament(){
		$this->db->select('A.id, A.date_ini , A.date_end, A.date_delivery, A.motivo, A.observation, B.username, C.serial_n, D.model, E.type');
		$this->db->from('er_reservations A');
		$this->db->join('ah_utilizadores_login B', 'A.user_id = B.id_utilizadores');
		$this->db->join('er_equipments C', 'A.equipment_id = C.id');
		$this->db->join('er_models D', 'C.model_id = D.id');
		$this->db->join('er_types E', 'D.type_id = E.id');
		return $this->db->get();
	}

	//Show all the locations of the equipaments
	public function location_equipament(){
		$this->db->select("er_e.id, er_t.type, er_b.brand, er_m.model, fc_f.nome_filial");
		$this->db->from('er_equipments er_e');
		$this->db->join('er_models er_m','er_m.id = er_e.model_id');
		$this->db->join('er_types er_t', 'er_m.type_id = er_t.id');
		$this->db->join('er_brands er_b','er_b.id = er_m.brand_id');
		$this->db->join('fc_filial fc_f', 'fc_f.id_filial = er_e.location_id');
		$this->db->where('er_e.b_delete', 0);
		$this->db->group_by('er_e.id');
		return $this->db->get();
	    
	}

	//Function to insert reservations
	public function insert_reservation($data)
	{
		return $this->db->insert('er_reservations', $data);
	}







	//********************************************************************
	private function _get_datatables_query()
    {
        $this->db->select('A.id, A.availability_id, A.location_id, A.model_id, A.b_delete, B.availability, C.location, D.model, E.type, F.brand, H.color');
        $this->db->from('er_equipments A');
        $this->db->join('er_availabilities as B', 'A.availability_id=B.id', 'inner');
        $this->db->join('er_acin_locations as C', 'A.location_id=C.id', 'inner');
        $this->db->join('er_models as D', 'A.model_id=D.id', 'inner');
        $this->db->join('er_types as E', 'D.type_id=E.id', 'inner');
        $this->db->join('er_brands as F', 'D.brand_id=F.id', 'inner');
        $this->db->join('er_models_has_colors as G', 'D.id=G.model_id', 'inner');
        $this->db->join('er_colors as H', 'G.color_id=H.id', 'inner');
        $this->db->where('A.b_delete', 0);
        $this->db->group_by('A.id');
        
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


	function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
//******************************************************************************



    function get_data_receive()
    {
        $this->_get_datatables_receive();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_receive()
    {
        $this->db->select('A.id, A.availability_id, A.location_id, A.model_id, A.b_delete, B.availability, C.location, D.model, E.type, F.brand, H.color');
        $this->db->from('er_equipments A');
        $this->db->join('er_availabilities as B', 'A.availability_id=B.id', 'inner');
        $this->db->join('er_acin_locations as C', 'A.location_id=C.id', 'inner');
        $this->db->join('er_models as D', 'A.model_id=D.id', 'inner');
        $this->db->join('er_types as E', 'D.type_id=E.id', 'inner');
        $this->db->join('er_brands as F', 'D.brand_id=F.id', 'inner');
        $this->db->join('er_models_has_colors as G', 'D.id=G.model_id', 'inner');
        $this->db->join('er_colors as H', 'G.color_id=H.id', 'inner');
        $this->db->where('A.b_delete', 0);
        $this->db->where('A.availability_id !=', 1);
        $this->db->group_by('A.id');
        
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    

	//********************************************************************
}


