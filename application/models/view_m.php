<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_m extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function view($view=null, $data=null, $access=null)
	{
        if(!$data){
            $this->load->view('Templates/header', $access);
        } else {
            $this->load->view('Templates/header',$data, $access);
        }
        $this->load->view($view);
        $this->load->view('Templates/footer');
    }
    
    public function view_admin($view=null, $data=null,$access=null)
	{
        if(!$data){
            $this->load->view('Templates/header', $access);
        } else {
            $this->load->view('Templates/header',$data, $access);
        }
        $this->load->view($view);
        $this->load->view('Templates/footer');
	}
}
