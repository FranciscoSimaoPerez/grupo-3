<div class="container-center">
  
    <div class="jumbotron element">
      <div class="title">Equipamentos</div>
      <hr>
        <div class="categories">
          <!--<div>
            <a href="<?php echo base_url('adicionar_c/add');?>"><button type="button" class="btn btn-primary float-right" >Adicionar Equipamento</button></a>
          </div>-->
          <!--Search input-->
          <div id="custom-search-input">
            <div class="input-group">
                <input type="text" class="search-query form-control search" placeholder="Search" />
                <button class="btn btn-primary search" type="submit">Search</button>
            </div>
          </div>

          <table class="table table-hover">
            <thead class="table-primary">
              <tr>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">Cor</th>
                <th scope="col">Localização</th>
                <th scope="col">Serial Nº.</th>
                <th scope="col">Disponibilidade</th>
                <th scope="col">Reservar</th>
              </tr>
            </thead>
            
            <tbody>
              <?php foreach ($equipments as $equip): ?>
              <tr>
                <td><?php echo $equip->brand; ?></td>
                <td><?php echo $equip->model; ?></td>
                <td><?php echo $equip->color; ?></td>
                <td><?php echo $equip->location; ?></td>
                <td><?php echo $equip->id_equip; ?></td>
                <td><?php echo $equip->availability; ?></td>
                <td><a href="<?php echo base_url('/reservar_c/create_reserve/'.$equip->id); ?>" class="reservar_btn">Reservar</a></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
    </div>
  
</div>