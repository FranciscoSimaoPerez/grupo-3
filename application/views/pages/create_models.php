
<!--DIV for the content of the center-->
<div class="container-center">
<!--DIV for add models-->
  <div class="jumbotron element">
    <div class="title">Criar Equipamentos</div>
    <hr>
    <!--Declaration of variables-->
    <?php
    //Variables for the input modelo
    $modelo = array(
      'class' => 'form-control',
      'id'    => 'modelo',
      'name'  => 'modelo',
      'required' => 'required'
    );
    
    //Variables for dropdown types
    $types = array(
      'class' => 'form-control',
      'id'    => 'opt-types',
      'name'  => 'opt-types'
    );
    $opt_types = array_combine(
      array_column($type, 'id'), 
      array_column($type, 'type')
    );

    //Variables for the dropdown brands
    $marca = array(
      'class' => 'form-control',
      'id'    => 'opt-brand',
      'name'  => 'opt-brand'
    );
    $opt_marca = array_combine(
      array_column($brand, 'id'), 
      array_column($brand, 'brand')
    );

    $create = array(
      'name'  => 'create',
      'id'    => 'create-model',
      'type'  => 'button',
      'class' => 'btn btn-primary',
      'value' => 'Criar modelo'
    );
    ?>
      <div class="container">
        <div class="alert alert-danger msg-error-js"  style="text-align:left;">
                  <strong>¡Importante!</strong> Corregir los siguientes errores.
                  <div class="list-errors"></div>
              </div>
        <form id="form-create-model" class="form-horizontal" role="form" action="<?php echo base_url();?>adicionar_c/create_model" method="POST">
          <div class="form-group row">

            <!--Input models-->
            <?php echo form_label('Modelo'); ?>
            <?php echo form_input($modelo)?>
          </div>
          
          <!--Dropdown Types-->
          <div class="form-group row">
            <?php echo form_label('Categoria'); ?>
            <?php echo form_dropdown($types, $opt_types)?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-types"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </div>

          <!--Dropdown Brands-->
          <div class="form-group row">
            <?php echo form_label('Marca')?>
            <?php echo form_dropdown($marca, $opt_marca)?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-brand"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
    </div>
    <div>
      <!--Button to create a model-->
      <?php echo form_input($create); ?>
    </div>
    <div>
      <a href="<?php echo base_url('adicionar_c/add'); ?>"><button class='btn btn-primary'  style="margin-top: 10px" type="button">Back</button></a>
    </div>
      <?php echo form_close(); ?>

<!--***********************Modal to insert brands**************************-->
  <div class="modal fade" id="create-brand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Criar marcas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">

              <div class="alert alert-danger msg-error-js"  style="text-align:left;">
                  <strong>¡Importante!</strong> Corregir los siguientes errores.
                  <div class="list-errors"></div>
              </div>
              <form id="form-create-brand" style="padding:0px 15px;"class="form-horizontal" role="form" action="<?php base_url();?>adicionar_c/brand" method="POST">
                <div class="form-group">
                  <input type="text" name="brand" id="input-brand" class="form-control" placeholder="Marcas" required />
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-primary btn-block " id="btn-brand" value="Registrar">Registrar</button>
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
<!--********************Modal to insert Loclizations**************************-->

<!--***********************Modal to insert Types***********************-->
  <div class="modal fade" id="create-types" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Criar Categoria</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">

              <div class="alert alert-danger msg-error-js"  style="text-align:left;">
                  <strong>¡Importante!</strong> Corregir los siguientes errores.
                  <div class="list-errors"></div>
              </div>
              <form id="form-create-types" style="padding:0px 15px;"class="form-horizontal" role="form" action="<?php base_url();?>adicionar_c/type" method="POST">
                <div class="form-group">
                  <input type="text" name="type" id="type" class="form-control" placeholder="Disponibilidade" required />
                </div>
                <div class="form-group">
                  <button type="button" id="reg-type" class="btn btn-primary btn-block" value="Registrar">Registrar</button>
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--***********************Modal to insert Types***********************-->
</div>
</div>