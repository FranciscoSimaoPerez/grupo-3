
<!--DIV for the content of the center-->
<div class="container-center">
<!--DIV for add equipments-->
  <div class="jumbotron element">

    <!--Div for the alert-->
    <div class="alert alert-danger msg-error-js"  style="text-align:left;">
      <strong>Importante!</strong> Corrija os erros seguintes.
          <div class="list-errors"></div>
    </div>
    
    <div class="title">Adicionar Equipamentos</div>
    <hr>
    <!--Declaration of variables-->
    <?php
    //Variables for the dropdown modelo
    $modelo = array(
      'class' => 'form-control',
      'id'    => 'opt-model',
      'name'  => 'opt-model'

    );
    $opt_model = array_combine(
      array_column($model, 'id'),
      array_column($model, 'model')
    );

    //Variables for dropdown Localização
    $loc = array(
      'class' => 'form-control',
      'id'    => 'opt-loc',
      'name'  => 'opt-loc'
    );
    $opt_loc = array_combine(
      array_column($location, 'id_filial'), 
      array_column($location, 'nome_filial')
    );

    //Variables for the dropdown disponibilidade
    $disp = array(
      'class' => 'form-control',
      'id'    => 'opt-disp',
      'name'  => 'opt-disp'
    );
    $opt_disp = array_combine(
      array_column($availability, 'id'), 
      array_column($availability, 'availability')
    );

    //Variables for cores
    $cores = array(
      'class' => 'form-control',
      'id'    => 'opt_colors_1',
      'name'  => 'opt_colors_1'
    );

    $opt_cores_1 = array_combine(
      array_column($colors, 'id'),
      array_column($colors, 'color')
    );

    $button = array(
      'name'  => 'create',
      'id'    => 'create_equipment',
      'type'  => 'button',
      'class' => 'btn btn-primary',
      'value' => 'Adicionar Equipamento'
    );
    ?>
      <div class="container">
        <form id="form-create-equipment" class="form-horizontal" role="form" action="<?php echo base_url();?>adicionar_c/create_equipment" method="POST">
        <!--Input Serial N.-->
          <div class="form-group row">
            <?php echo form_label('Serial N.'); ?>
            <input class="form-control" id="serial" type="text" name="serial" value="" placeholder="Serial Number" required>
          </div>

        <!--Dropdown Modelos-->
          <div class="form-group row">
            <?php echo form_label('Modelo'); ?>
            <?php echo form_dropdown($modelo, $opt_model)?>
            <a href="<?php echo base_url('show_model_properties'); ?>" style="text-decoration:none;"><button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
          </div>

          <!--Dropdown localization-->
          <div class="form-group row">
            <?php echo form_label('Localização'); ?>
            <?php echo form_dropdown($loc, $opt_loc)?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-localization"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </div>

          <!--Dropdown Disponibilidade-->
          <div class="form-group row">
            <?php echo form_label('Disponibilidade')?>
            <?php echo form_dropdown($disp, $opt_disp)?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-item"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </div>

          <div class="form-group row">
            <?php echo form_label('Cores')?>
            <?php echo form_dropdown($cores, $opt_cores_1)?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-colors"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
    </div>
    <div>
      <?php echo form_input($button); ?>
      <?php echo form_close(); ?>
    </div>



<!--***********************Modal to insert Colors***********************-->
  <div class="modal fade" id="create-colors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Criar Colors</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">

              <div class="alert alert-danger msg-error-js"  style="text-align:left;">
                  <strong>Importante!</strong> Corrija os erros seguintes.
                  <div class="list-errors"></div>
              </div>
              <form id="form-create-colors" class="form-horizontal" role="form" action="<?php echo base_url();?>adicionar_c/colors" method="POST">
                <div class="form-group">
                  <input type="text" name="input_color" id="input_color" class="form-control" placeholder="Color" required />
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-primary btn-block" id="btn-color" value="Registrar">Registrar</button>
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--***********************Modal to insert Colors***********************-->


<!--***********************Modal to insert Disponibilidade***********************-->
  <div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Criar Disponibilidade</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">

              <div class="alert alert-danger msg-error-js"  style="text-align:left;">
              <strong>Importante!</strong> Corrija os erros seguintes.
                  <div class="list-errors"></div>
              </div>
              <form id="form-create-usuario" style="padding:0px 15px;"class="form-horizontal" role="form" action="<?php base_url();?>adicionar_c/disponibilidade" method="POST">
                <div class="form-group">
                  <input type="text" name="disponibilidade" id="disponibilidade" class="form-control" placeholder="Disponibilidade" required />
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-primary btn-block bt-submit-js" value="Registrar">Registrar</button>
                </div>
              </form>

        </div>
      </div>
    </div>
  </div>
</div>
<!--***********************Modal to insert Disponibilidade***********************-->


<!--***********************Modal to insert Loclizations**************************-->
  <div class="modal fade" id="create-localization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Criar localizações</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">

              <div class="alert alert-danger msg-error-js"  style="text-align:left;">
                  <strong>Importante!</strong> Corrija os erros seguintes.
                  <div class="list-errors"></div>
              </div>
              <form id="form-create-localization" style="padding:0px 15px;"class="form-horizontal" role="form" action="<?php base_url();?>adicionar_c/localization" method="POST">
                <div class="form-group">
                  <input type="text" name="local" id="input-loc" class="form-control" placeholder="Localizações" required />
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-primary btn-block " id="loc" value="Registrar">Registar</button>
                </div>
              </form>

        </div>
      </div>
    </div>
  </div>
<!--********************Modal to insert Loclizations**************************-->
</div>
</div>

<script >

  
</script>