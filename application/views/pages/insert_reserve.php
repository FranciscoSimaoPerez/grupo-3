<div class="container-center">
  <div class="jumbotron element">
    
    <!--****************Flashdata*****************-->
    <?php if($this->session->flashdata('reservation_error')): ?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('reservation_error').'</p>'; ?>
    <?php endif; ?>
    <!--****************Flashdata*****************-->

    <!--***********Equipment selected**************-->
    <div class="title">Equipamento Escolhido</div>
    <hr>
      <div class="categories">
        <div class="card bg-light mb-3" style="max-width: 20rem;">
            <div class="card-body">
              <?php foreach ($equip as $equip):?>
              <p class="card-text">Marca : <?php echo $equip->brand?></p>
              <p class="card-text">Modelo : <?php echo $equip->model; ?></p>
              <p class="card-text">Cor : <?php echo $equip->color; ?> </p>
              <p class="card-text">Serial Nº : <?php echo $equip->id_equip; ?> </p>
            
            </div>
        </div>
      </div>
      <!--***********Equipment selected**************-->

      <!--******Form to reserve the equipment*********-->

      <form action="<?php echo base_url('reservar_c/insert_reservation/').$equip->id_equip; ?>" method="post">
        <?php endforeach; ?>
        <div class="container">
          <div class="title">Dados da Reserva</div>
          <hr>
          
          <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Motivo</label>
            <input type="text" class="form-control" placeholder="Introduza Um Motivo" name="motivo" required="" >
          </div>
        </div>

        <div class="data_card">
          <div class="container">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Data de Reserva</label>
              <input class="datapicker" id="datepicker" data-provide="datepicker" name="date_in" required >

                <script>
                  
                  var date = new Date();
                  var date = new Date();
                  var currentMonth = date.getMonth();
                  var currentDate = date.getDate();
                  var currentYear = date.getFullYear();

                  date.setDate(date.getDate());

                  $('#datepicker').datepicker({
                      format: 'yyyy/mm/dd',
                      minDate: new Date(currentYear, currentMonth, currentDate)
                  });
                </script>
            </div>
          </div>

          <div class="container">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Data de Entrega</label>
              <input class="datapicker" id="datepicker1" data-provide="datepicker" name="date_out" required >

                <script>
                  
                  var date = new Date();
                  var date = new Date();
                  var currentMonth = date.getMonth();
                  var currentDate = date.getDate();
                  var currentYear = date.getFullYear();

                  date.setDate(date.getDate());

                  $('#datepicker1').datepicker({
                      format: 'yyyy/mm/dd',
                      minDate: new Date(currentYear, currentMonth, currentDate)
                  });
                </script>
            </div>

            <div>
              <button type="submit" class="btn btn-primary">Reservar</button> 
            </div>

          </div>

        </div>
      </form>
      <!--******Form to reserve the equipment*********-->

      
  </div>
</div>
