<div class="container">
    <div class="jumbotron mb-3">
        <h3 class="text-center mb-5">
            Eliminar Equipamento
        </h3>
        <div class="table-responsive">
            <?php
            $this->table->set_heading("Marca", "Modelo", "Cor", "Localização", "Tipo", "Disponibilidade");
            $id                 = $equipment->id_equip;
            $marca      		= $equipment->brand;
            $modelo     		= $equipment->model;
            $cor                = $equipment->color;
            $localizacao        = $equipment->location;
            $disponibilidade	= $equipment->availability;
            $tipo		  		= $equipment->type;
            if ($disponibilidade == 'Disponível') {
                $disp = "<i style='color:green;' class='fas fa-lg fa-check-circle'></i>";
            } else if ($disponibilidade == 'Manutenção'){
                $disp = "<i style='color:orange;' class='fas fa-lg fa-times-circle'></i>";
            } else {
                $disp = "<i style='color:crimson;' class='fas fa-lg fa-times-circle'></i>";
            }

            
            $this->table->add_row($marca, $modelo, $cor, $localizacao, $disp, $tipo);
            $this->table->set_template(array(
                'table_open' => '<table class="table table-hover text-center">',
                'thead_open' => '<thead class="bg-primary text-light">',
            ));
            echo $this->table->generate();
            ?>
        </div>
    
    
        <p>Tem a certeza que deseja eliminar este equipamento?</p>
        <?php echo anchor(base_url('edit'), 'Cancelar','class="btn btn-secondary"'); ?>
        <?php echo anchor(base_url('edit/confirm-delete-equip/'.$equipment->id_equip), 'Eliminar','class="btn btn-danger"'); ?>


    </div>
</div>