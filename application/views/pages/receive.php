<div class="container-center">
  <div class="jumbotron element">
    <div class="title">Reservas Pendentes</div>
    <?php if($this->session->flashdata('reservation_success')): ?> 
	        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('reservation_success').'</p>'; ?>
	    <?php endif; ?>
    
    <!--*****Table*****-->
        <div class="table">
          <table id="recibimentos" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead class="table-primary">
                <tr>
					<th scope="col">Marca</th>
		            <th scope="col">Modelo</th>
		            <th scope="col">Cor</th>
		            <th scope="col">Categoria</th>
                    <th scope="col">Localização</th>
		            <th scope="col">Disponibilidade</th>
                    <th style="width:150px;">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            
        </table>
        </div>
    <!--*****Table*****-->









    
  </div>
</div>

<script>
	$(document).ready(function() {
    //datatables
    table = $('#recibimentos').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('adicionar_c/receive_list'); ?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
                "defaultContent": "<i>Not set</i>"
            },

        ],


    });
});


  </script>