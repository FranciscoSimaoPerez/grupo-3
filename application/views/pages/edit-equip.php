<div class="container">
    <div class="jumbotron mt-4">
        <h1>Editar equipamento</h1>
        <?php 
            echo validation_errors('<div class="alert-danger">','</div>');
            echo form_open(base_url('edit/save-equipment-changes/'.$equipment->id), 'class="d-flex flex-column justify-content-center id="equipment_form"');
        ?>

        <input type="hidden" name="txt-id" value="<?php echo $equipment->id_equip; ?>">

        <div class="form-group">
            <label for="type">Tipo de Equipamento:</label>
            <select type="text" class="form-control" id="type" disabled>
                <option>
                    <?php echo $equipment->type; ?>
                </option>
            </select>
        </div>
        <div class="form-group">
            <label for="brand">Marca:</label>
            <select type="text" class="form-control" id="brand" disabled>
                <option>
                    <?php echo $equipment->brand; ?>
                </option>
            </select>
        </div>
        <div class="form-group">
            <label for="model">Modelo:</label>
            <select type="text" class="form-control" id="model" disabled>
                <option>
                    <?php echo $equipment->model; ?>
                </option>
            </select>
        </div>
        <div class="form-group">
            <label for="type">Disponibilidade</label>
            <select type="text" name="select-availability" class="form-control" id="type" required>
                <?php
                            foreach ($availabilities as $availability) {
                                if($equipment->availability_id == $availability->id){
                        ?>
                <option value="<?php echo $availability->id; ?>" selected="selected">
                    <?php echo $availability->availability; ?>
                </option>
                <?php
                                } else {
                        ?>
                <option value="<?php echo $availability->id; ?>">
                    <?php echo $availability->availability; ?>
                </option>
                <?php
                                }
                                
                            }
                        ?>
            </select>
        </div>
        <div class="form-group">
            <label for="type">Localização</label>
            <select type="text" name="select-location" class="form-control" id="type" required>
                <?php
                    foreach ($locations as $location) {
                        if($equipment->location_id == $location->id){
                ?>
                <option value="<?php echo $location->id; ?>" selected="selected">
                    <?php echo $location->location; ?>
                </option>
                <?php
                        } else {
                ?>
                <option value="<?php echo $location->id; ?>">
                    <?php echo $location->location; ?>
                </option>
                <?php
                        }            
                    }
                ?>
            </select>
        </div>
        <button class="btn btn-primary btn-sm my-0 p" type="submit">
            Alterar
        </button>
        <?php echo form_close(); ?>
    </div>
</div>