<!DOCTYPE html>
<html>
<head>
  <title>RE | Acin</title>
  <link rel="shortcut icon" href="https://www.acin.pt/img/brand/logotipo.png" />

  <!-- Font Awesome -->
   <link href="<?php echo base_url('/assets/css/font-awesome.min.css')?>" rel="stylesheet">


  <!-- Custom Theme Style -->
  <script src="<?php echo base_url('/assets/js/ajax_emocoes.js')?>"></script>

  <link href="<?php echo base_url('/assets/css/custom.min.css')?>" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="<?php echo base_url('/assets/css/bootstrap.min.css')?>" rel="stylesheet">

  <link href="<?php echo base_url('/assets/css/bootstrap-theme.min.css')?>" rel="stylesheet">

  <!-- CSS -->
  <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="<?php echo base_url('/assets/js/jquery.min.js')?>"></script>

<!-- DESCARGAR -->
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- Datatables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>


  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



</head>

<body>
<input type="hidden" value="<?=base_url()?>" id="base_url">
<!--DIV principal container-->
<div class="container-page">
  <!--DIV for the header-->
  <div class="container-header">

    <!-- Navbar -->
    <div class="top_nav">
      <div class="nav_menu">
        <nav >
          <div class="nav toggle">
            <a id="menu_toggle" href="<?php echo base_url('/reservar_c'); ?>" ><i class="fa fa-bars"></i></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><?php 
                
                echo (isset($user) && $user!=null)?$user:""?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php 
                    if ($access_level == 3 || $access_level == 2) { ?>
                      <li><a class="dropdown-item" href="<?php echo base_url('adicionar_c/receive');?>">Reservas Pendentes</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('adicionar_c/add');?>">Criar Equipamento</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('/edit_c');?>">Editar Equipamento</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('adicionar_c/req_equipament');?>">Requisições do Equipamento</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('adicionar_c/loc_equipament');?>">Localização do Equipamento</a></li>
                    <?php
                    }
                    
                    elseif ($access_level == 1) { ?>
                      <li><a class="nav-link" href="<?php echo base_url('/reservar_c'); ?>">Home</a></li>    
                      <li><a class="dropdown-item" href="<?php echo base_url('/reservar_c/requisitions'); ?>">As Minhas Requisições</a></li>
                    <?php
                    }
                    ?>
                  <!-- <li><a href="<?php echo base_url('/acinhub/perfil')?>">Perfil</a></li>
                  <li><a href="<?php echo base_url('/acinhub/editar')?>">Editar perfil</a></li> -->
                  
                  
                  <li><a href="<?php echo base_url('../home/publico/logout')?>">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    <!-- Canvas full page -->

  <canvas id="canvas"></canvas>

</div>