<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| AcinHub url
|--------------------------------------------------------------------------
|
| URL to acin hub base installation
|
*/
$config['acin_hub']    = 'http://192.168.1.100/hub/';

/*
|--------------------------------------------------------------------------
| AcinHub url
|--------------------------------------------------------------------------
|
| URL to acin hub home installation
|
*/
$config['acin_hub_home']	= $config['acin_hub'] .'home/';

/*
|--------------------------------------------------------------------------
| AcinHub login url
|--------------------------------------------------------------------------
|
| This is the url where users with an account on acinhub will be able to
| login and acces to AcinHub
|
*/
$config['acin_hub_login']	= $config['acin_hub_home'].'user/login';

/*
|--------------------------------------------------------------------------
| AcinHub logout url
|--------------------------------------------------------------------------
|
| This is the url where users must be redirected in order to logout from AcinHub
|
*/
$config['acin_hub_logout']	= $config['acin_hub_home'].'user/logout';
/*
|--------------------------------------------------------------------------
| AcinHub webapp id
|--------------------------------------------------------------------------
|
| This is an unique id that identify your webapp  in acinhub, this id must be
| delivered by the acinhub administrator upon your webapp registration
|
*/
$config['acin_hub_web_app_id']	= 8;


/*
|--------------------------------------------------------------------------
| AcinHub user profile
|--------------------------------------------------------------------------
|
| This is the url that links to the personal user profile in acinhub
|
*/
$config['acin_hub_user_profile'] = $config['acin_hub_home'].'acinhub/perfil';


/*
|--------------------------------------------------------------------------
| Defines the default expirancy time for the cookie when logging in with
| remember me option set to true
|--------------------------------------------------------------------------
|
| Time expirancy is defined in seconds
| Default : 2629743 ( 1 month)
|
*/
$config['acin_hub_remember_me_cookie_expirancy'] = 2629743; //1 month
